#!/bin/bash

########################################################################
#
#   ------------------------------------------------------------------
#  |                                                                  |
#  |   Personal ICS files generator (from ADE extracted global ICS)   |
#  |                     Version 1.9 (04.07.2024)                     |
#  |                                                                  |
#   ------------------------------------------------------------------
#
#   Copyright (C) 2024 Serguei Dachian
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   This program is a bash script, which besides "bash" requires the
#   standard utilities (including "grep" and "sed"), as well as unicode
#   support, in order to run.  It also recommends "zip" in order to
#   output ICS files as a single ZIP archive.  It was tested on Linux,
#   on Mac OS and on Cygwin equipped Windows, but should also work on
#   any POSIX compliant OS having the required tools.
#
#   The script must be run with exactly one argument: the name of an ADE
#   extracted global ICS file to be used as input.  For example
#   (supposing the script "pICSgen.sh" has the executable flag set):
#
#   ./pICSgen.sh MON_FICHIER_ADE.ics
#
#   The generated personal ICS files will be contained in a single ZIP
#   archive named "Personal_ICS_files.zip" if "zip" is available, and in
#   the "Personal_ICS_files" folder otherwise.
#
#   WARNING: beware that if such a ZIP archive or folder exists, it will
#   be overwritten by the script without prompting.
#
########################################################################

# Checking arguments
#
if [ $# -ne 1 ]
  then
    echo
    echo "ERROR: exactly one argument (input file name) must be supplied!"
    echo
    exit 1
fi

INFILE="$1"
OUTNAME="Personal_ICS_files"
TMPDIR="./pICSgen_`date +%s`_$RANDOM"
LIST=""

# Checking the input file
#
if [ ! -r "$INFILE" ]
  then
    echo
    echo "ERROR: the file \"$INFILE\" is not readable!"
    echo
    exit 2
fi

if ! grep "BEGIN:VCALENDAR" "$INFILE" &> /dev/null
  then
    echo
    echo "ERROR: the file \"$INFILE\" seems not to be an ICS file!"
    echo
    exit 3
fi

# Starting
#
echo
echo "Starting..."
echo

# Checking OS type
#
case $OSTYPE in
  linux*|cygwin*|darwin*)
    WARNS=0
    ;;
  *)
    echo "WARNING: running on an untested OS!"
    echo
    WARNS=1
    ;;
esac

# Creating a temporary directory for output files
#
rm -rf "$TMPDIR"

echo -n "Creating temporary directory... "

if mkdir -p "$TMPDIR/$OUTNAME" &> /dev/null
  then
    echo "Done."
    echo
  else
    echo "FAIL!"
    echo
    echo "ERROR: mkdir failed!"
    echo
    rm -rf "$TMPDIR"
    exit 4
fi

# Loop on persons' names
#
while IFS= read -r ICSNAME
do
  #
  # correcting ADE temporary names
  #
  SHORTNAME=`echo "$ICSNAME" | sed "s/ ([A-Z '\-]*) / /g" | sed "s/ ([0-9]*) / /g" | sed 's/ \[[0-9]*\] / /g' | sed 's/ *X[0-9][0-9]*//g'`
  ICSNAME=`echo "$ICSNAME" | sed 's/\[/\\\[/g' | sed 's/\]/\\\]/'`
  #
  # replacing spaces and apostrophes with underscores in filenames
  #
  OUTFILE=`echo "$SHORTNAME.ics" | sed 's/ /_/g' | sed "s/'/_/g"`
  LIST="$LIST $OUTFILE"
  echo -n "Generating the file \"$OUTFILE\"... "
  #
  # converting CRLF to LF, joining broken lines, delimiting with < and >,
  # changing BEGIN to PICSGEN_BEGIN in matching blocks, removing other blocks,
  # removing delimiting (and changing PICSGEN_BEGIN back to BEGIN),
  # correcting ADE temporary names and removing empty lines
  #
  cat "$INFILE" | sed $'s/\r$//' | sed 'H;1h;$!d;x;s/\n //g' | sed 's/END\:VEVENT/END\:VEVENT\>/g' | sed 's/BEGIN\:VEVENT/\<BEGIN\:VEVENT/g' | sed "H;1h;\$!d;x;s/<BEGIN\:VEVENT\([^>]*$ICSNAME[^>]*>\)/\<PICSGEN_BEGIN\:VEVENT\1/g" | sed 'H;1h;$!d;x;s/<BEGIN\:VEVENT[^>]*>//g' | sed 's/END\:VEVENT>/END\:VEVENT/g' | sed 's/<PICSGEN_BEGIN\:VEVENT/BEGIN\:VEVENT/g' | sed "s/ ([A-Z '\-]*) / /g" | sed "s/ ([0-9]*) / /g" | sed 's/ \[[0-9]*\] / /g' | sed 's/ *X[0-9][0-9]*//g' | grep -v '^$' > "$TMPDIR/$OUTNAME/$OUTFILE"
  #
  # braking back the long lines
  #
  while grep '^.\{74\}' "$TMPDIR/$OUTNAME/$OUTFILE" &> /dev/null
    do
    sed $'s/^\\(.\\{73\\}\\)\\(..*\\)$/\\1\\\n \\2/' "$TMPDIR/$OUTNAME/$OUTFILE" > "$TMPDIR/$OUTNAME/$OUTFILE.tmp"
    mv -f "$TMPDIR/$OUTNAME/$OUTFILE.tmp" "$TMPDIR/$OUTNAME/$OUTFILE"
    done
  #
  # converting LF back to CRLF
  #
  sed $'s/$/\r/' "$TMPDIR/$OUTNAME/$OUTFILE" > "$TMPDIR/$OUTNAME/$OUTFILE.tmp"
  mv -f "$TMPDIR/$OUTNAME/$OUTFILE.tmp" "$TMPDIR/$OUTNAME/$OUTFILE"
  #
  # Checking the output file
  #
  if [ -s "$TMPDIR/$OUTNAME/$OUTFILE" ]
    then
      echo "Done."
   else
      echo "WARNING: the file \"$OUTFILE\" is empty!"
      let "WARNS=WARNS+1"
  fi
  #
done << END_OF_LIST
$(cat "$INFILE" | sed $'s/\r$//' | sed 'H;1h;$!d;x;s/\n //g' | grep '^DESCRIPTION\:' | sed 's/^DESCRIPTION\://' | sed 's/\\n.*-CM-Grp[0-9]*//g' | sed 's/\\n.*-CMTD-Grp[0-9]*//g' | sed 's/\\n.*-TD-Grp[0-9]*//g' | sed 's/\\n.*-TP.Sciences-Grp[0-9]*//g' | sed 's/\\n.*-CM//g' | sed 's/\\n.*-CM.TD//g' | sed 's/\\n.*-TD//g' | sed 's/\\n.*-TP.Sciences//g' | sed 's/Semestre.[0-9]*\\n//g' | sed 's/Z--[A-Za-z0-9-]*\\n//g' | sed 's/\\n(Export.*)//g' | sed $'s/\\\\n/\\\n/g' | grep -v '^$' | sort -u)
END_OF_LIST
#
# List of persons' names for while loop is generated (above) as follows:
# converting CRLF to LF, joining broken lines, keeping only DESCRIPTION lines,
# removing groupes (CM, CTD, TD and TP), super-groupes (Z--), semesters and
# export date, changing "\n"s to real line breaks, removing empty lines,
# sorting with -u

# Checking the number of files
#
TMPA=$(expr `echo "$LIST" | wc -w` + 0)
TMPB=$(expr `ls -1 "$TMPDIR/$OUTNAME" | wc -l` + 0)
if [ $TMPA -ne $TMPB ]
  then
    echo
    echo "ERROR: something went wrong!?"
    echo
    rm -rf "$TMPDIR"
    exit 5
fi
if [ $TMPA -eq 0 ]
  then
    echo
    echo "ERROR: no files were generated!"
    echo
    rm -rf "$TMPDIR"
    exit 6
fi
echo
echo "$TMPA files were generated."
echo

# Zipping
#
echo -n "Zipping files... "

cd "$TMPDIR" &> /dev/null
if zip -r "$OUTNAME.zip" "$OUTNAME" &> /dev/null
  then
    echo "Done."
    echo
    cd - &> /dev/null
    rm -f "$OUTNAME.zip"
    if ! cp "$TMPDIR/$OUTNAME.zip" "$OUTNAME.zip" &> /dev/null
      then
        echo "ERROR: unable to copy the output to the \"$OUTNAME.zip\" file!"
        echo
        rm -rf "$TMPDIR"
        exit 7
    fi
  else
    echo "WARNING: zip failed, recovering the output as a folder!"
    echo
    let "WARNS=WARNS+1"
    cd - &> /dev/null
    rm -rf "$OUTNAME"
    if ! cp -r "$TMPDIR/$OUTNAME" "$OUTNAME" &> /dev/null
      then
        echo "ERROR: unable to copy the output to the \"$OUTNAME\" folder!"
        echo
        rm -rf "$TMPDIR"
        exit 8
    fi
fi

# Finishing
#
case $WARNS in
  0) echo "Finished successfully." ;;
  1) echo "Finished with 1 WARNING." ;;
  *) echo "Finished with $WARNS WARNINGS." ;;
esac

echo
rm -rf "$TMPDIR"
exit 0
