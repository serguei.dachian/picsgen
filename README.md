# Personal ICS files generator (from ADE extracted global ICS)

## Version 1.8 (03.01.2024)

### Copyright (C) 2024 Serguei Dachian

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

This program is a bash script, which besides "bash" requires the standard utilities (including "grep" and "sed"), as well as unicode support, in order to run.  It also recommends "zip" in order to output ICS files as a single ZIP archive.  It was tested on Linux, on Mac OS and on Cygwin equipped Windows, but should also work on any POSIX compliant OS having the required tools.

The script must be run with exactly one argument: the name of an ADE extracted global ICS file to be used as input.  For example (supposing the script "pICSgen.sh" has the executable flag set):

```
./pICSgen.sh MON_FICHIER_ADE.ics
```

The generated personal ICS files will be contained in a single ZIP archive named "Personal_ICS_files.zip" if "zip" is available, and in the "Personal_ICS_files" folder otherwise.

WARNING: beware that if such a ZIP archive or folder exists, it will be overwritten by the script without prompting.

The latest stable version of the script can be run directly from GitLab (requires "wget") as follows:

```
wget -qO- https://gitlab.univ-lille.fr/serguei.dachian/picsgen/-/raw/master/pICSgen.sh | bash -s MON_FICHIER_ADE.ics
```

Similarly, to run the latest development version of the script, you can use:

```
wget -qO- https://gitlab.univ-lille.fr/serguei.dachian/picsgen/-/raw/dev/pICSgen.sh | bash -s MON_FICHIER_ADE.ics
```
